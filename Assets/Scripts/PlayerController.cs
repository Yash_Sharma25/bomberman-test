﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{                               // For movement
    public float speed = 2.0f;                        // Speed of movement
     
    Animator anim;

    Rigidbody2D rigidbody;
    GameController gc;

    Vector3 pos;


    void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
             // Take the initial position
        pos = transform.position;
     }
 
    void Update(){

        if (Input.GetKey(KeyCode.D) && transform.position == pos && !isWall(gc.tileMap.WorldToCell(pos+Vector3.right)))
        {
            pos += Vector3.right;
        }
        else if (Input.GetKey(KeyCode.A) && transform.position == pos && !isWall(gc.tileMap.WorldToCell(pos+Vector3.left)))
        {
            pos += Vector3.left;
        }
        else if (Input.GetKey(KeyCode.W) && transform.position == pos && !isWall(gc.tileMap.WorldToCell(pos+Vector3.up)))
        {
            pos += Vector3.up;
        }
        else if (Input.GetKey(KeyCode.S) && transform.position == pos && !isWall(gc.tileMap.WorldToCell(pos+Vector3.down)))
        {
            pos += Vector3.down;
        }
    
    }

    void FixedUpdate(){

        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);
        
    }

    bool isWall(Vector3Int cell){
        
        Tile tile = gc.tilemap.GetTile<Tile>(cell);

        if(tile == gc.wallTile || tile == gc.destructibleTile){
            return true;
        }

        return false;
    }
        

    void OnCollisionEnter2D(Collision2D collision){
        if(collision.collider.tag == "Enemy" || collision.collider.tag == "Explode"){
            PlayDead();
            gc.GameOver();
        }
    }

    void OnTriggerEnter2D(Collider2D collider){
    
        if(collider.tag == "Explode" ){
            gc.GameOver();
        }

    }

    void PlayDead(){
        Debug.Log("Dead");
        //play dead animation
    }
}
