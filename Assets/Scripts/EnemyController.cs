﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EnemyController : MonoBehaviour
{   
    public float speed;

    GameController gc;
    
    Vector3 moveVelocity;

    int direction;

    Vector3 moveInput;

    bool findingNextDirection = true;

    bool moving = false;

    Vector3 goal;

    int idle_wait = 0;


	public static Vector3 roundedWormPos; 

    Animator anim;

    Vector3 pos;

    Vector3 directionVector; 

    bool killed = false;

    void Start ()
    {
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        goal = transform.position;
        pos = transform.position;
        directionVector = Vector3.right;
        // InvokeRepeating("DirectionChange", 0f, 10f);
    }

    void FixedUpdate() {
        if(!isWall(gc.tileMap.WorldToCell(pos+directionVector))){
            pos+=directionVector;
        }

        else
            ChangeDirection();
        
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);

    }

     void ChangeDirection(){

            Vector3 pos = transform.position;
            if(!isWall(gc.tileMap.WorldToCell(pos+Vector3.up))){
                directionVector = Vector3.up;
            }

            else if(!isWall(gc.tileMap.WorldToCell(pos+Vector3.down))){
                directionVector = Vector3.down;
            }
            
            else if(!isWall(gc.tileMap.WorldToCell(pos+Vector3.left))){
                directionVector = Vector3.left;
            }
            
            else if(!isWall(gc.tileMap.WorldToCell(pos+Vector3.right))){
                directionVector = Vector3.right;
            }


    }

    bool isWall(Vector3Int cell){
            
            Tile tile = gc.tilemap.GetTile<Tile>(cell);

            if(tile == gc.wallTile || tile == gc.destructibleTile){
                return true;
            }

            return false;
    }



    /* int DirectionChange(){

        int r = direction;
        while(r == direction){
            r = Random.Range(1,4);
        }

        direction = r;

        switch(direction){
            //right
            case 1:
                directionVector = Vector3.right;
                break;

            //down
            case 2:
                directionVector = Vector3.down;
                break;

            //left
            case 3:
                directionVector = Vector3.left;
                break;

            //up
            case 4:
                directionVector = Vector3.up;
                break;
        }
        return direction;
    }*/

    void OnTriggerEnter2D(Collider2D collider){
    
        if(collider.tag == "Explode" && !killed){
            killed = true;
            gc.UpdateScore(100);
            Destroy(this.gameObject);
        }

    }

   void OnCollisionEnter2D(Collision2D collision){
        if(collision.collider.tag == collision.otherCollider.tag){
            // gc.UpdateScore(100);
            Physics2D.IgnoreCollision(collision.collider, collision.otherCollider);
        }
    }
}




		