using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

	public float countdown = 2f;
	
	GameController gc;

	void Start(){
		gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}

	// Update is called once per frame
	void Update () {
		countdown -= Time.deltaTime;

		if (countdown <= 0f)
		{
			FindObjectOfType<MapDestroyer>().Explode(transform.position);
			gc.SetCharge(false);
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
       if(collider.tag == "Player")
            gc.GameOver();

        if(collider.tag == "Enemy")
            Debug.Log("Destroy enemy");
            // DestroyEnemy();
   }
}
