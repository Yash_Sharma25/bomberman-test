﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    private float w = 15;
    private float h = 10;

    private Rigidbody rb;

    private GameObject bomb;
    
    public float speed;
    public GameObject player;
    public Tilemap tileMap;

    public int destructiblesCount;
    public int maxEnemies;

    private bool chargeSet;
    public Vector3 bombPosition;
    private int enemyCount;

    public GameObject bombPrefab;
    public GameObject enemyPrefab;
    public GameObject destructiblePrefab;
	public Tilemap tilemap;

    public Tile wallTile;
	public Tile destructibleTile;


    public TextMeshProUGUI score;

    public GameObject overScreen;

    void Start ()
    {
        //Maybe not Rigidbody
        rb = GetComponent<Rigidbody>();
        enemyCount = maxEnemies;
        chargeSet = false;
        StartCoroutine(Init());
    }

    void FixedUpdate ()
    {
        if(Input.GetKeyDown(KeyCode.Space) && ! chargeSet){
            //Spawn bomb
            bombPosition = player.transform.position;
            chargeSet = true;
            SpawnBomb();

        }
    }

    IEnumerator Init(){
        //the player is already placed

        //ProduceDestructibles
          
        /*for(int i = 0; i < destructiblesCount; i++){
            
            //make this random
            Vector3 destructiblePosition = new Vector3(Random.Range(0, w), Random.Range(0, h), 0);
            Vector3Int cell = tileMap.WorldToCell(destructiblePosition);
            Vector3 cellCenterPosition = tileMap.GetCellCenterWorld(cell);
            GameObject destructible = Instantiate(destructiblePrefab, cellCenterPosition, Quaternion.identity);
        
        }*/

        //SpawnEnemies
        for(int i = 0; i < maxEnemies; i++){
            Vector3Int cell;
            do{
                Vector3 enemyPosition = new Vector3(Random.Range(-3,5), Random.Range(-5, 5), 0);
                cell = tileMap.WorldToCell(enemyPosition);
            }while(isWall(cell));

            Vector3 cellCenterPosition = tileMap.GetCellCenterWorld(cell);
            GameObject enemy = Instantiate(enemyPrefab, cellCenterPosition, Quaternion.identity);
        
        }

        yield return null;
    }

    public bool isWall(Vector3Int cell){
        
        Tile tile = tilemap.GetTile<Tile>(cell);

        if(tile == wallTile || tile == destructibleTile){
            return true;
        }

        return false;
    }

    void SpawnBomb(){
    
        Vector3Int cell = tileMap.WorldToCell(bombPosition);
        Vector3 cellCenterPosition = tileMap.GetCellCenterWorld(cell);
        GameObject bomb = Instantiate(bombPrefab, cellCenterPosition, Quaternion.identity);
        
    }
    
    public void UpdateScore(int hitScore){
        // score.value += hitScore;
        Debug.Log("Update score");
        int currentScore = System.Int32.Parse(score.text.Split(':')[1]);
        score.text = "Score:" + (currentScore + hitScore);
        enemyCount--;
        Debug.Log("EnemyCount = "+enemyCount);

         if(enemyCount == 0){
            PlayerWins();
        }
    }

    void PlayerWins(){
        Time.timeScale = 0;
        overScreen.SetActive(true);
        overScreen.transform.Find("win").gameObject.SetActive(true);

        //show game win screen
    }

    public void GameOver(){
        Time.timeScale = 0;
        overScreen.SetActive(true);
        overScreen.transform.Find("gameOver").gameObject.SetActive(true);
        //show game over screen
    }

    void NextRound(){
        SceneManager.LoadScene("scene0");
    }

    public void SetCharge(bool set){
        chargeSet = set;
    }

    void DisplayScore()	{

		/*Rect scoreRect = new Rect(10, 10, 32, 32);
		GUI.DrawTexture (scoreRect, scoreIconTexture);

		GUIStyle style = new GUIStyle ();
		style.fontSize = 30;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.yellow;

		Rect labelRect = new Rect (scoreRect.xMax + 10, scoreRect.y, 60, 32);
		GUI.Label (labelRect, score.ToString (), style);*/

	}

    void OnGUI(){
        DisplayScore();
    }
}
