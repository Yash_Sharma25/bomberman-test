using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapDestroyer : MonoBehaviour {

	public Tilemap tilemap;

	public Tile wallTile;
	public Tile destructibleTile;

	public GameObject explosionPrefab;

	GameObject explosion;
	
	int bombRange = 2;

	Vector3Int originCell;
	public void Explode(Vector2 worldPos)
	{
		originCell = tilemap.WorldToCell(worldPos);

        //explode the bomb cell and the 3 adjacent cells
        ExplodeCell(originCell);

        bool shouldContinue;

        //right = 1, bottom = 1, left = -1, top = -1

        //top
        ExplodeFull( 0, -1);
        
        //bottom
        ExplodeFull( 0, 1);

        //left
        ExplodeFull( -1, 0);

        //right
        ExplodeFull( 1, 0);

        //not to go through the other wall	

	}

	void ExplodeFull(int x, int y){

        for(int i = 0; i <= bombRange; i++){

            if(!ExplodeCell(originCell + new Vector3Int(x * i, y * i, 0)))
                break;
                
        }
    }

	bool ExplodeCell (Vector3Int cell)
	{
		Tile tile = tilemap.GetTile<Tile>(cell);

		if (tile == wallTile)
		{
			return false;
		}

		if (tile == destructibleTile)
		{
			tilemap.SetTile(cell, null);
		}

		Vector3 pos = tilemap.GetCellCenterWorld(cell);
		GameObject explosion = Instantiate(explosionPrefab, pos, Quaternion.identity);
		Destroy(explosion,0.5f);
		return true;
	}

}
